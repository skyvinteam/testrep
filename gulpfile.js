var gulp  = require('gulp'),
    sass = require('gulp-sass');

gulp.task('scss', function () {
    return gulp.src('common.blocks/common.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('css'));
});

gulp.task('scss:watch',function () {
    gulp.watch('common.blocks/**/*.scss', ['scss']);
});

gulp.task('start', ['scss', 'scss:watch']);